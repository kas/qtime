#!/usr/bin/env miniscript
// -*- mode: miniscript; coding: utf-8 -*-

///////////////////////////////////////////////////////////////////////////////
// Copyright ©1997-2017 Klaus Alexander Seistrup <klaus@seistrup.dk>
//
// Version: 0.0.1 (2024-07-13)
//
// SPDX-License-Identifier: GPL-3.0-or-later
//
// This program is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation; either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but with-
// out any warranty; without even the implied warranty of merchantability or
// fitness for a particular purpose. See the GNU General Public License for
// more details.  <http://gplv3.fsf.org/>
///////////////////////////////////////////////////////////////////////////////

import "dateTime"

the_hours = ["twelve", "one", "two", "three", "four", "five",
             "six", "seven", "eight", "nine", "ten", "eleven"]

now = dateTime.nowVal
seconds = (dateTime.hour(now) * 60 + dateTime.minute(now)) * 60 + dateTime.second(now)
minutes = floor((seconds + 30) / 60) + 27
hours = floor(minutes / 60)
minutes %= 60
oclock = "It's " + ["nearly ", "almost ", "", "just after ", "after "][minutes % 5]
divisions = floor(minutes / 5) - 5

if divisions then
  oclock += ["", "five ", "ten ", "a quarter ", "twenty ", "twenty-five ", "half "][abs(divisions)]
  if divisions < 0 then
    oclock += "to "
  else
    oclock += "past "
  end if
end if

oclock += the_hours[hours % 12]

if not divisions then
  oclock += " o'clock"
end if

print(oclock + ".")

// eof
