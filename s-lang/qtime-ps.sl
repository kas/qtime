#!/usr/bin/env slsh
%% -*- mode: slang; coding: utf-8 -*-

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% This is just qtime.sl in S-Lang's postscript notation
%%
%% Copyright ©2024 Klaus Alexander Seistrup <klaus@seistrup.dk>
%%
%% Version: 0.0.1 (2024-01-27)
%%
%% SPDX-License-Identifier: GPL-3.0-or-later
%%
%% This program is free software; you can redistribute it and/or modify it
%% under the terms of the GNU General Public License as published by the Free
%% Software Foundation; either version 3 of the License, or (at your option)
%% any later version.
%%
%% This program is distributed in the hope that it will be useful, but with-
%% out any warranty; without even the implied warranty of merchantability or
%% fitness for a particular purpose. See the GNU General Public License for
%% more details.  <http://gplv3.fsf.org/>
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Because sometimes the seconds' part will be "08" or "09"
. ( "1" exch strcat integer 100 - ) nn2ii

% Returns current local time as an English sentence
. ( [TP NY MN HR now hh mm ss adjust hours minutes divisions tiktok]
.   "to ::past " =TP
.   "nearly :almost ::just after :after " =NY
.   ":five :ten :a quarter :twenty :twenty-five :half " =MN
.   "twelve:one:two:three:four:five:six:seven:eight:nine:ten:eleven" =HR
.   time " " strcompress 3 ' ' extract_element =now
.   now 0 ':' extract_element nn2ii =hh
.   now 1 ':' extract_element nn2ii =mm
.   now 2 ':' extract_element nn2ii =ss
.   hh 60 * mm + 60 * ss + 30 + 60 / 27 + int =adjust
.   adjust 60 / 12 mod int =hours
.   adjust 60 mod =minutes
.   minutes 5 / 5 - int =divisions
.   NY minutes 5 mod ':' extract_element
.   MN divisions abs ':' extract_element
.   TP divisions sign 1 + ':' extract_element
.   HR hours ':' extract_element
.   strcat strcat strcat =tiktok
.   divisions { "$tiktok o'clock"$ =tiktok } !if
.   tiktok
. ) qtime

% Do you have the time?
. [when]
. qtime =when
. "It's $when."$ message

%% eof
