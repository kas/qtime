#!/usr/bin/env tclsh
# -*- tcl -*-

##############################################################################
# Copyright ©2023 Klaus Alexander Seistrup <klaus@seistrup.dk>
#
# Version: 0.0.1 (2023-08-23)
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 3 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but with-
# out any warranty; without even the implied warranty of merchantability or
# fitness for a particular purpose. See the GNU General Public License for
# more details. <http://gplv3.fsf.org/>
##############################################################################

proc qtime {} {
   set the_hours [list "twelve" "one" "two" "three" "four" "five" \
                       "six" "seven" "eight" "nine" "ten" "eleven"]
   set the_minutes [list "nearly " "almost " "" "just after " "after "]
   set the_fives [list "" "five " "ten " "a quarter " \
                       "twenty " "twenty-five " "half "]
   set now [clock seconds]
   set ss [string trimleft [clock format $now -format "%S"] "0"]
   set mm [string trimleft [clock format $now -format "%M"] "0"]
   set hh [string trimleft [clock format $now -format "%H"] "0"]
   set seconds [expr {($hh * 60 + $mm) * 60 + $ss}]
   set minutes [expr {($seconds + 30) / 60 + 27}]
   set hours [expr {$minutes / 60}]
   set minutes [expr {$minutes % 60}]
   set divisions [expr {$minutes / 5 - 5}]
   set elms [list [lindex $the_minutes [expr {$minutes % 5}]]]
   if $divisions {
      set elms [list {*}$elms [lindex $the_fives [expr {abs($divisions)}]]]
      set elms [list {*}$elms [expr $divisions < 0 ? "{to }" : "{past }"]]
   }
   set elms [list {*}$elms [lindex $the_hours [expr {$hours % 12}]]]
   if !$divisions {
      set elms [list {*}$elms " o'clock"]
   }
   return [join $elms ""]
}

puts "It's [qtime]."

# eof
