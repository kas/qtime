#[                                                              -*- nim -*-
##   Version: qtime-nim/0.0.1 (2023-03-24)
## Copyright: © 2023 Klaus Alexander Seiﬆrup <klaus@seistrup.dk>
##   License: GNU Affero General Public License v3+
##  Git repo: https://codeberg.org/kas/qtime
]#

import std/strutils
import std/times

let HOURS = [
  "twelve", "one", "two", "three", "four", "five",
  "six", "seven", "eight", "nine", "ten",  "eleven"
]

let FIVES = [
  "", "five ", "ten ", "a quarter ", "twenty ", "twenty-five ", "half "
]

let MINUTES = [
  "nearly ", "almost ", "", "just after ", "after "
]

proc qtime(): string =
  var
    minutes: int
    elms: seq[string]

  let now = now()
  let seconds = (now.hour * 60 + now.minute) * 60 + now.second

  minutes = ((seconds + 30) div 60) + 27
  let hours = minutes div 60
  minutes = minutes mod 60
  let divisions = (minutes div 5) - 5
  elms = @["It's ", MINUTES[minutes mod 5]]

  if divisions != 0:
    add(elms, FIVES[abs(divisions)])
    if divisions < 0:
      add(elms, "to ")
    else:
      add(elms, "past ")

  add(elms, HOURS[hours mod 12])

  if divisions == 0:
    add(elms, " o'clock")

  add(elms, ".")

  return elms.join("")

if isMainModule:
  echo qtime()

# eof
